#!/bin/bash

#
# Build Script for making standalone version of Tesseract based on the work of Wes Fowlks
# Peter Nagy
# 26/04/2016
# Originally posted at:https://code.google.com/p/tesseract-ocr/issues/detail?id=1326
#

BUILD_ZLIB=1
BUILD_LIBJPEG=1
BUILD_LIBPNG=1
BUILD_LEPTONICA=1
BUILD_TESSERACT=1

# Get the base directory of where the script is
BASE_DIR=${1%/}
BUILD_DIR="${BASE_DIR:?}"/build
ARCHIVE_DIR="${BASE_DIR:?}"/archives
SRC_DIR="${BASE_DIR:?}"/src
TESSERACT_DIR="${BASE_DIR:?}"/tesseract

#Library Versions
ZLIB_VERSION=1.2.8
LIBPNG_VERSION=1.6.23
LIBJPEG_VERSION=9b
LEPTONICA_VERSION=1.73
TESSERACT_VERSION=3.02.02
TESSDATA_VERSION=3.04.00

# Variables and functions for coloring output

black='\E[30;40m'
red='\E[31;40m'
green='\E[32;40m'
blue='\E[34;40m'
white='\E[37;40m'
bold='\033[1m'
nobold='\033[0m'

function Reset {
         tput -T screen sgr0      #  Reset text attributes to normal
                                  #+ without clearing screen.
}


function cecho (){                # Color-echo. Argument $1 = message,  Argument $2 = color
        local default_msg="No message passed."
                                  # Doesn't really need to be a local variable.

        message=${1:-$default_msg}   # Defaults to default message.
        color=${2:-$black}           # Defaults to black, if not specified.

        echo -e -n "$color"
        echo "$message"
        Reset                      # Reset to normal.

        return
}

function cbecho(){
    echo -e -n "$bold"
    cecho "$1" "$2"
    echo -e "$nobold"
    return
}

# Functions usefull throughtout the script
function setupDirs() {
        if [[ ! -d "${ARCHIVE_DIR:?}" ]]; then
                mkdir -p "${ARCHIVE_DIR:?}"
        fi

        if [[ ! -d "${SRC_DIR:?}" ]]; then
                mkdir -p ${SRC_DIR:?}
        fi

        if [[ ! -d "${BUILD_DIR:?}" ]]; then
                mkdir -p ${BUILD_DIR:?}
        fi
}

function downloadSrc(){
        if [[ ! -f "${ARCHIVE_DIR:?}/$1" ]]; then
                cecho "Downloading $1" $white
                cecho "(from $2)"
                curl -L -o "${ARCHIVE_DIR:?}"/$1 $2
        fi

        if [[ ! -f "${ARCHIVE_DIR:?}/$1" ]]; then
            cbecho "Download of $1 failed. Exiting!" $red
            exit 1
        fi
}

function extractSrc(){
        cecho "Extracting archive" $white
        tar -xzf "${ARCHIVE_DIR:?}/$1" -C ${SRC_DIR:?}
}

function getSrc(){
        downloadSrc $1 $2
        extractSrc  $1
}

function checkBuild(){
        if [[ -f $1 ]]; then
                cbecho "$2 Build Successful" $green
        else
                cbecho "$2 build failed. Exiting." $red
                exit 1
        fi

}

function cleanUp(){
        rm -rf "${SRC_DIR:?}/$1 ${BUILD_DIR:?}/$1"
}

cbecho "Base Build Directory: ${BUILD_DIR:?}" $white

# First check to see if zlib
if [[ "${BUILD_ZLIB:?}" = 1 ]]
then
        cbecho "Building ZLIB" $white
        setupDirs

        # Clean up old files
        cleanUp "zlib*"

        getSrc "zlib-${ZLIB_VERSION:?}.tar.gz" "http://zlib.net/zlib-${ZLIB_VERSION:?}.tar.gz"

        cd "${SRC_DIR:?}/zlib-${ZLIB_VERSION:?}"

        cecho "Configuring ZLIB for Standalone" $white
        ./configure --solo

        cecho "Building Zlib and deploying to ${BUILD_DIR:?}" $white
        make install prefix=${BUILD_DIR:?}

        checkBuild "${BUILD_DIR:?}/include/zlib.h" "ZLIB"
else
        cecho "Skipping ZLib" $blue
fi

# Build Libjpeg
if [[ ${BUILD_LIBJPEG:?} = 1 ]]
then

        cbecho "Building Lib Jpeg" $white
        setupDirs

        cleanUp "jpeg*"

        getSrc "jpegsrc.v${LIBJPEG_VERSION:?}.tar.gz" "http://www.ijg.org/files/jpegsrc.v${LIBJPEG_VERSION:?}.tar.gz"

        cd "${SRC_DIR:?}/jpeg-${LIBJPEG_VERSION:?}"

        cecho "Configuring Lib Jpeg for Standalone" $white
        ./configure --prefix=${BUILD_DIR:?}

        cecho "Building LIBJPEG and deploying to ${BUILD_DIR:?}" $white
        make install

        checkBuild "${BUILD_DIR:?}/include/jpeglib.h" "LIB JPEG"
else
        cecho "Skipping LIBJPEG" $blue
fi

# Build Lib PNG
if [[ ${BUILD_LIBPNG:?} = 1 ]]
then
        cbecho "Building Lib PNG" $white
        setupDirs

        cleanUp "libpng*"

        getSrc "libpng-${LIBPNG_VERSION:?}.tar.gz" "http://downloads.sourceforge.net/project/libpng/libpng16/older-releases/${LIBPNG_VERSION:?}/libpng-${LIBPNG_VERSION:?}.tar.gz?use_mirror=tcpdiag"

        cd "${SRC_DIR:?}/libpng-${LIBPNG_VERSION:?}"

        cecho "Copying libz header files to libpng" $white
        cp ${BUILD_DIR:?}/include/zlib.h .
        cp ${BUILD_DIR:?}/include/zconf.h .

        cecho "Configuring Lib PNG for Standalone" $white
        ./configure --prefix=${BUILD_DIR:?}

        cecho "Building LIBPNG and deploying to ${BUILD_DIR:?}" $white
        make check
        make install

        checkBuild "${BUILD_DIR:?}/include/libpng16/png.h" "LIB PNG"
else
        cecho "Skipping LIBPNG" $blue
fi

# Build Leptonica
if [[ ${BUILD_LEPTONICA:?} = 1 ]]
then
        cbecho "Building Leptonica" $white
        setupDirs

        # Clean up old files
        cleanUp "leptonica*"

        getSrc "leptonica-${LEPTONICA_VERSION:?}.tar.gz" "http://www.leptonica.com/source/leptonica-${LEPTONICA_VERSION:?}.tar.gz"

        cd "${SRC_DIR:?}/leptonica-${LEPTONICA_VERSION:?}"

#        cecho "Configuring leptonica for standalone" $white
#        ./make-for-local

        CFLAGS=-O2 ./configure --prefix=${BUILD_DIR:?}
        cecho "Modifying environ.h" $white
        cat src/environ.h |sed -e 's/#define  HAVE_LIBTIFF     1/#define  HAVE_LIBTIFF     0/g' > src/environ.test.h
        mv src/environ.test.h src/environ.h

        cecho "Copying dependencies to leptonica" $white
        cp -r ${BUILD_DIR:?}/include src
        cd src

        cecho "Building LEPTONICA and deploying to ${BUILD_DIR:?}" $white
        make EXTRAINCLUDES="-I./include -I./include/libpng16"
        make install

        checkBuild "${BUILD_DIR:?}/lib/liblept.a" "Leptonica"

        if [[ ! -f "${BUILD_DIR:?}/include/leptonica" ]]; then
                mkdir ${BUILD_DIR:?}/include/leptonica
        fi

        cp ${SRC_DIR:?}/leptonica-${LEPTONICA_VERSION:?}/src/*.h ${BUILD_DIR:?}/include/leptonica

else
        cecho "Skipping Leptonica" $blue
fi

# Build Tesseract
if [[ ${BUILD_TESSERACT:?} = 1 ]]
then

        cbecho "Building Tesseract" $white
        cleanUp "tesseract*"

        #Create Tesseract Build Directory
        if [[ ! -d "${TESSERACT_DIR:?}" ]]; then
                mkdir "${TESSERACT_DIR:?}"
        else
                rm -rf "${TESSERACT_DIR:?/}"*
        fi

        getSrc "tesseract-ocr-${TESSERACT_VERSION:?}.tar.gz" "https://github.com/tesseract-ocr/tesseract/archive/${TESSERACT_VERSION:?}.tar.gz"

        cd "${SRC_DIR:?}/tesseract-${TESSERACT_VERSION:?}"

        cp -r ${BUILD_DIR:?}/include src
        cp -r ${BUILD_DIR:?}/bin src
        cp -r ${BUILD_DIR:?}/lib src

        cecho "Configuring Tesseract" $white
        ./autogen.sh
        ./configure --prefix=${TESSERACT_DIR:?} --with-extra-libraries=${BUILD_DIR:?}/lib \
        CXPFLAGS="-I${BUILD_DIR:?}/include -I${BUILD_DIR:?}/include/libpng16 -I${BUILD_DIR:?}/include/leptonica -lpng -ljpeg -lz" \
        LDFLAGS="-L${BUILD_DIR:?}/lib" \
        LIBLEPT_HEADERSDIR="${BUILD_DIR:?}/include/leptonica" \

        cecho "Configuration done, now Building" $white
        make install

        checkBuild "${TESSERACT_DIR:?}/bin/tesseract" "Tesseract"

        cbecho "Checking the tessdata files" $white
        downloadSrc "tesseract-ocr-${TESSDATA_VERSION:?}.tar.gz" "https://github.com/tesseract-ocr/tessdata/archive/${TESSDATA_VERSION:?}.tar.gz"

        cecho "Installing Languages and OSD" $white
        mkdir "${TESSERACT_DIR:?}"/tessdata

        tar -xzf "${ARCHIVE_DIR:?}"/tesseract-ocr-${TESSDATA_VERSION:?}.tar.gz -C ${TESSERACT_DIR:?}
        mv ${TESSERACT_DIR:?}/tessdata-${TESSDATA_VERSION:?} ${TESSERACT_DIR:?}/tessdata

        cbecho "Tesseract is now built and can be found at: ${TESSERACT_DIR:?}" $green

else
        cecho "Skipping Tesseract" $blue
fi
